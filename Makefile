clean-neo4j-db:
	rm -rf .local/neo4j/data/databases/amundsen.db
	docker-compose -f docker-amundsen-local.yml restart neo4j
