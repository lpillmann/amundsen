# Amundsen setup for Thinksurance

This documentation describes technical aspects of how to use this repository to setup Amundsen for Thinksurance.
## Documentation
Complete documentation available [in Confluence](https://gewerbeversicherung24.atlassian.net/l/c/Sup5wuoN).

## Sync with Amundsen's original repository (GitHub)
This repository in Bitbucket is in sync with the original in Github. Reference ([link](https://stackoverflow.com/a/15105821)).

Verify the two existing remotes (github and bitbucket)
```bash
git remote -v
```

Sync github branch with updates from original repo
```bash
git checkout github
git pull
```

Incorporate updates to our branch
```bash
git checkout main
git rebase github
```
> **NOTE**: make sure a thorough QA is done on the github version before merging. In the past there were some bugs in their up-to-date code which broke the frontend.

## Setup
### Environment variables
Define the following environment variables (change the values accordingly - e.g. to point to a different cluster machine)
```bash
# ELASTICSEARCH
export AMUNDSEN_ELASTICSEARCH_HOST='localhost'
export AMUNDSEN_ELASTICSEARCH_PORT=9200

# NEO4J
export AMUNDSEN_NEO4J_PORT=7687
export AMUNDSEN_NEO4J_ENDPOINT='bolt://localhost:7687'
export AMUNDSEN_NEO4J_USER='admin'
export AMUNDSEN_NEO4J_PASSWORD='admin'

# HIVE
export AMUNDSEN_HIVE_METASTORE_CONNECTION_STRING='postgresql://hive@thanos.thinksurance.de:5432/hive'
```
In the production server it was defined in a `.env` file in the directory `/prod_tsbigdata/99_infra_sector/quality_assurance/tsamundsen/src/job`.

### Launch with Docker
Use docker-compose to start all the services
```bash
sudo docker-compose -f docker-amundsen-local.yml up -d
```
Amundsen will be at port 5000. Neo4j admin console will be at port 7474.
> **NOTE**: the first time it runs it can take several minutes to build the Docker images. 


## Neo4j Backup
Download APOC plugin and place it into plugins folder
```bash
cd example/docker/neo4j/plugins
wget https://github.com/neo4j-contrib/neo4j-apoc-procedures/releases/download/3.5.0.14/apoc-3.5.0.14-all.jar
```

Create backup files (from root directory)
```bash
bash scripts/create_neo4j_backup.sh
```

Recover from backup files
```bash
bash scripts/recover_from_neo4j_backup.sh <backup-date>
```
where `<backup-date>` refers to an existing backup in S3 in the format `YYYY_mm_dd`, e.g. `2021_10_11`.

Then, update the Elasticsearch index (run from the machine where Amundsen is deployed - e.g. Ancient)
```bash
cd databuilder
# install dependencies
source venv/bin/activate
pip install -e .
# run elasticsearch update script
python example/scripts/update_elasticsearch_index.py
```
