#!/bin/bash

# Create Neo4j backup files and store in S3
# Usage:
#   - From root folder: `bash scripts/create_neo4j_backup.sh` 

set -e

backup_files_base_path="./example"
backup_files_directory_path="backup"
schema_backup_filename="amundsen_schema.cypher"
data_backup_filename="amundsen_data.graphml"
today=$(date '+%Y_%m_%d')

compressed_filepath="./tmp/amundsen_backup.tar.gz"
s3_bucket="s3://tsdatalake"
s3_base_path="amundsen_backup"
s3_destination_path="$s3_bucket/$s3_base_path/$today/"
# Set credentials
aws_key="$AWS_ACCESS_KEY_ID"
aws_secret="$AWS_SECRET_ACCESS_KEY"

# This step is done as a paliative solution to "Out of Memory Error" when doing the backup (suggested/aligned with Cleber on 2022-04-27)
echo "STEP 0: Restart Presto to free up RAM"
bash -c "/opt/presto*/bin/launcher restart"

echo "STEP 1: Backup neo4j schema"
sudo docker exec neo4j_amundsen bash \
cypher-shell -u neo4j -p test \
"CALL apoc.export.cypher.schema('/$backup_files_directory_path/$schema_backup_filename')"

echo "STEP 2: Backup neo4j data"
sudo docker exec neo4j_amundsen bash \
cypher-shell -u neo4j -p test \
"CALL apoc.export.graphml.all('/$backup_files_directory_path/$data_backup_filename', {useTypes: true, readLabels: true})"

echo "STEP 3: Store backups in S3"
echo "STEP 3.1: Compress files"
sudo tar -zcvf "$compressed_filepath" "$backup_files_base_path/$backup_files_directory_path"
echo "Successfully compressed to $compressed_filepath"

echo "STEP 3.2: Copy to S3"
aws s3 cp "$compressed_filepath" "$s3_destination_path"
