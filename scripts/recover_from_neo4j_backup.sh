#!/bin/bash

# Create Neo4j backup files and store in S3
# Usage:
#   - From root folder: `bash scripts/recover_from_neo4j_backup.sh.sh <backup-date>`
#     where <backup-date> is in the format YYYY_mm_dd e.g. 2021_10_11
#

set -e

backup_date=$1

backup_files_base_path="./example"
backup_files_directory_path="backup"
data_backup_filename="amundsen_data.graphml"

compressed_filename="amundsen_backup.tar.gz"
compressed_filepath="./tmp/$compressed_filename"
s3_bucket="s3://tsdatalake"
s3_base_path="amundsen_backup"
s3_destination_path="$s3_bucket/$s3_base_path/$backup_date/$compressed_filename"


if [ $# -eq 0 ]
  then
    echo "Error: No arguments supplied, please provide a backup date to be recovered from (example 2021_01_01)"
    exit
fi

echo "STEP 1: Recover backups in S3"
echo "STEP 1.2: Download from S3"
aws s3 cp "$s3_destination_path" "$compressed_filepath"

echo "STEP 1.1: Extract files"
tar -zxvf "$compressed_filepath" "$backup_files_base_path/$backup_files_directory_path"
echo "Successfully extracted to $backup_files_base_path/$backup_files_directory_path"

echo "STEP 2: Recovering neo4j data from backup"
sudo docker exec neo4j_amundsen bash \
cypher-shell -u neo4j -p test \
"CALL apoc.import.graphml('/$backup_files_directory_path/$data_backup_filename', {useTypes: true, readLabels: true})"
